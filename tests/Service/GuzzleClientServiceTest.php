<?php

namespace App\Tests\Service;

use App\Service\GuzzleClient\Client;
use PHPUnit\Framework\TestCase;

class GuzzleClientServiceTest extends TestCase
{
    /**
     * @var Client
     */
    private $service;

    /**
     * Tests get Weather as stdclass
     */
    public function testGetWeather()
    {
        $response = $this->service->getWeather('London');

        $this->assertTrue($response->cod === 200);
        $this->assertTrue($response->name === 'London');
        $this->assertEquals(1, count($response->weather));
    }
}