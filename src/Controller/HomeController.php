<?php declare(strict_types=1);

namespace App\Controller;

use App\Service\GuzzleClient\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiIconUrl;

    public function __construct(Client $client, string $apiIconUrl)
    {
        $this->client = $client;
        $this->apiIconUrl = $apiIconUrl;
    }

    /**
     * Main page with Weather Widget
     *
     * @Route("/", name="home_index")
     */
    public function index()
    {
        $weather = $this->client->getWeather('New York,usa');
//        $weather = $this->client->getWeather('London,GB');
//        $weather = $this->client->getWeather('Lublin,pl');
//        $weather = $this->client->getWeather('Paris,FR');
//        $weather = $this->client->getWeather('Dubai');

        return $this->render('test/index.html.twig', [
            'iconsUrl' => $this->apiIconUrl,
            'weatherData' => $weather,
            'openWeateherApi' => 'OpenWeatherMap API',
        ]);
    }
}